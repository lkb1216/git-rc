# git-rc
```
👻 Read(r) and Create(c) Git Repositories
```
This is a wrapper library of [nodegit](https://github.com/nodegit/nodegit) to provide below functions more conveniently.
* listing branches
* reading a file tree of a branch
* reading contents of a specific file
* creating a new repository

## Requirements
You must meet the requirements of [nodegit](https://github.com/nodegit/nodegit) to use this library.   
Please visit and check [nodegit](https://github.com/nodegit/nodegit) before using this library.


## Install
```sh
npm install git-rc
```

## Usage
```js
const fs = require('fs')
const path = require('path')
const { spawn } = require('child_process')
const gitRC = require('git-rc')

const dir = path.join(__dirname, 'repos')
fs.mkdirSync(dir)

// create a Repostories instance
const repositories = gitRC(dir)


;(async () => {
  // create a repository
  await repositories.create({ name: 'repo', bare: false })
  
  // set up the above repository for a demonstration
  process.chdir(path.join(dir, 'repo'))
  fs.writeFileSync('a.txt', 'abcd')
  const promised = f => new Promise(f)
  await promised((a, r) => spawn('git', ['add', 'a.txt']).on('exit', c => c ? r(c) : a()))
  await promised((a, r) => spawn('git', ['commit', '-am', 'a!!']).on('exit', c => c ? r(c) : a()))
  
  // create a Repository instance
  const repository = await repositories.open('repo')
  
  // list branches
  const branches = await repository.branches()
  console.log(branches)
  
  // reads a file tree
  const filetree = await repository.filetree('master')
  console.log(JSON.stringify(filetree, null, 2))
  
  // reads file contents
  const file = await repository.file({ branch: 'master', path: '/a.txt'})
  console.log(file)
})()
```

## API
### [sync] gitRC(dir: String)
returns a <b>Repositories</b> instance

### [class] Repositories
[async] Repositories#<b>create</b> ({ name: String[, bare: Boolean = false ]  })   
[async] Repositories#<b>open</b> (name: String) => a <b>Repository</b> instance

### [class] Repository
[async] Repository#<b>branches</b> () => Array&lt;String&gt;   
[async] Repository#<b>filetree</b> (branch: String) => Array&lt;TreeItem&gt;
```
TreeItem: { name: String, path: String[, dir: Array<TreeItem> ] }
```
[async] Repository#<b>file</b> ({ branch: String, path: String }) => String 
